var Option = {
    diagramType:'echarts',
    textTitle:'公司发展历程',
    background:'businessProjectFirst',
    title: {
        text: '2018年',
        subtext: '',
        x:'center',
        top: 25,
        itemGap: 0,
        z: 200,
        textStyle: {
            color: '#fff'
        }
    },
    color:['#FB6A4B', '#2CF4F6'],
    legend: {
        right: '3%',
        bottom:'3%',
        width:'100%',
        orient: 'horizontal',
        icon: 'circle',
        data:['新中标','以往中标'],
        textStyle: {
            color: '#0000',
        }
    },
    series: [
        {
            name: '新中标',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle: {
                normal:{
                    areaColor: '#116064'
                }
            },
            label: {
                normal: {
                    show: true,
                    falsetextStyle: {
                        color: "#fff",
                        fontSize:8
                    }
                },
                emphasis: {
                    show: false
                }
            },
            data:[
                {name: '青海',value: 200},
                {name: '新疆',value: 200},
                {name: '四川',value: 200},
                {name: '云南',value: 200}
            ]
        },
        {
            name: '以往中标',
            type: 'map',
            mapType: 'china',
            label: {
                normal: {
                    show: false
                },
                emphasis: {
                    show: true
                }
            },
            data:[
                {name: '西藏',value: 200},
                {name: '湖北',value: 200},
                {name: '陕西',value: 200}
            ]
        }
    ]
};
export default Option;