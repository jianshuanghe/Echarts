import React, { Component } from 'react';
// 引入 ECharts 主模块
import echarts from 'echarts';
import Option from './echart';
import china from './china.json';
echarts.registerMap('china', china);

class App extends Component {
    componentDidMount() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        // 绘制图表
        myChart.setOption(
            Option
        );
    }
    render() {
        return (
            <div id="main" style={{ width: '100%', height: 400 }}></div>
        );
    }
}

export default App;